package com.nutes.review.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import com.nutes.review.model.User;

public interface UserRepository extends JpaRepository<User, Long> {

}
