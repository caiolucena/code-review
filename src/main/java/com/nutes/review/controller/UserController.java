package com.nutes.review.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import com.nutes.review.model.User;
import com.nutes.review.repository.UserRepository;


@RestController
public class UserController {

  @Autowired
  private UserRepository userRepository;

  @GetMapping
  public ResponseEntity<List<User>> retrieveAllUsers() {

    List<User> users = userRepository.findAll();

    return ResponseEntity.status(HttpStatus.OK).body(users);
    
  }
  
}
